addtags

Input spreadsheet is a tab separated spreadsheet named "spreadsheet.txt" in
your $HOME directory, which has the columns: 
    original file name TAB new file name TAB "tag1, tag2, ... ".
Note that lists of tags are enclosed in double quotes and separated by
commas.  The file names must not contain paths; the path to the files
is determined by the working directory when the script is executed.
Tags are added to the image files both as OS X finder tags, and as EXIF 
metadata tags (which are used by programs such as Lightroom).

1. Save spreadsheet as tab-separated text file named spreadsheet.txt
2. Move spreadsheet.txt to your home directory
3. Move addtags and addtags.py to your home directory
4. Install exiftool from https://www.sno.phy.queensu.ca/~phil/exiftool/
5. Change directory to one containing image files to be renamed and tagged
6. Execute ~/addtags
7. Repeat from 5 for any other image directories
