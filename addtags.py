import os
import subprocess
import pathlib
import re

# Spreadsheet.txt is a tab-separated spreadsheet in the home directory.
# col 1 = original file name (with extension), col 2 = new file name (without extension), col 3 = keywords
home = str(pathlib.Path.home())
f = open(home + "/spreadsheet.txt","r",encoding='utf-8',errors='ignore')
raw = f.readlines()
print('Read spreadsheet')

# Convert spreadsheet into a dictionary
table = [ s.strip().split('\t') for s in raw ]
dict = { d[0] : d[1:] for d in table }

# Process image files in the working directory
files = os.listdir()
print('Read directory, number of files = ', len(files))
for oldfile in files :

    # Look up the image file in the dictionary. If the file name does not
    # appear, also try stripping off leading four-digit and underscore
    # prefix from the file name.  If file still does not appear, then
    # ignore it.
    try:
        try:
            entry = dict[oldfile]
        except KeyError:
            entry = dict[re.sub('^[0-9][0-9][0-9][0-9]_', '', oldfile)]
    except KeyError:
        print("Skipping ", oldfile)
        continue

    # Rename the file, keeping the extension used by the original file.
    extension = os.path.splitext(oldfile)[1]
    newfile = re.sub('["/&\\\']',"",entry[0]) + extension
    print('Renaming ', oldfile, ' as ', newfile)
    os.rename(oldfile,newfile)

    # Add tags as EXIF keywords.  This allows tags to be used by programs such
    # as Adobe Lightroom.  If program exiftool is not installed, delete this section.
    rawtagstring = entry[1]
    keywordtag = '-keywords=' + rawtagstring
    print('  Inserting exif tags ', keywordtag)
    subprocess.call(['exiftool', "-m", "-overwrite_original", keywordtag, newfile])

    # Add tags as Finder tags. Delete this section if not running on a Mac.
    taglist = [ e.strip() for e in rawtagstring.strip('" ').split(',') ]
    tagstring = [ '<string>' + e + '</string>' for e in taglist ]
    tagstring = "".join(tagstring)
    tagstring = '<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd"><plist version="1.0"><array>' + tagstring + '</array></plist>'
    print('  Inserting OS X tags ', taglist)
    subprocess.call(['xattr', '-w', 'com.apple.metadata:_kMDItemUserTags', tagstring, newfile])

print('Done!')

